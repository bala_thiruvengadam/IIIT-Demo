'use strict';
// Declare app level module which depends on filters, and services
angular.module('myApp', [
        'ngRoute',
        "mobile-angular-ui",
        "mobile-angular-ui.touch",
        "mobile-angular-ui.scrollable",
        //'ngTouch',
        'myApp.filters',
        'myApp.services',
        'myApp.directives',
        'myApp.controllers'
        //'ui.bootstrap'
    ]).
    config(['$routeProvider', '$httpProvider', function($routeProvider, $httpProvider) {
        $routeProvider.when('/view1', {templateUrl: 'partials/partial1.html', controller: 'MyCtrl1'});
        $routeProvider.when('/view2', {templateUrl: 'partials/partial2.html', controller: 'MyCtrl2'});
        $routeProvider.when('/home', {templateUrl: 'partials/home.html', controller: 'HomeCtrl'});
        $routeProvider.when('/amakerhome', {templateUrl: 'partials/amakerhome.html', controller: 'AMakerHomeCtrl'});
        $routeProvider.when('/settings', {templateUrl: 'partials/settings.html', controller: 'SettingsCtrl'});
        $routeProvider.when('/login', {templateUrl: 'partials/login.html', controller: 'LoginCtrl'});
        $routeProvider.when('/device', {templateUrl: 'partials/device.html', controller: 'DeviceCtrl'});
        $routeProvider.when('/help', {templateUrl: 'partials/help.html', controller: 'HelpCtrl'});
        $routeProvider.when('/about', {templateUrl: 'partials/about.html', controller: 'AboutCtrl'});
        $routeProvider.when('/schoollist', {templateUrl: 'partials/schoollist.html', controller: 'SchoolListCtrl'});
        $routeProvider.when('/sendfeedback', {templateUrl: 'partials/feedback.html', controller: 'SendFeedbackCtrl'});
        $routeProvider.when('/reportissue', {templateUrl: 'partials/reportissue.html', controller: 'ReportIssueCtrl'});
        $routeProvider.when('/app/:appid/:pageid', {templateUrl: 'partials/app.html', controller: 'AppCtrl'});
        $routeProvider.when('/launch/:tenant', {templateUrl: 'partials/partial1.html', controller: 'LaunchCtrl'});
        $routeProvider.when('/applications', {templateUrl: 'partials/applications.html', controller: 'ApplicationsCtrl'});
        //$routeProvider.otherwise({redirectTo: '/amakerhome'});
        $routeProvider.otherwise({redirectTo: '/home'});
        $httpProvider.defaults.timeout = 5000;
    }])
    .service('analytics', [
    '$rootScope', '$window', '$location', function($rootScope, $window, $location) {
        var send = function(evt, data) {
            ga('send', evt, data);
        }
    }
]);

var onDeviceReady = function() {
    angular.bootstrap( document, ['myApp']);
    var handleOrientation = function() {
        if (orientation == 0) {
            if(MyCampusApp.homeScreenDisplayed) {
                MyCampusApp.currentPage = 1;
                setTimeout(function(){MyCampusApp.homeRoute.reload()},500);
            }
        } else if (orientation == 90) {
            if(MyCampusApp.homeScreenDisplayed) {
                MyCampusApp.currentPage = 1;
                setTimeout(function(){MyCampusApp.homeRoute.reload()},500);
            }
        } else if (orientation == -90) {
            if(MyCampusApp.homeScreenDisplayed) {
                MyCampusApp.currentPage = 1;
                setTimeout(function(){MyCampusApp.homeRoute.reload()},500);
            }
        } else if (orientation == 180) {
            if(MyCampusApp.homeScreenDisplayed) {
                MyCampusApp.currentPage = 1;
                setTimeout(function(){MyCampusApp.homeRoute.reload()},500);
            }
        } else {}
    }
    window.handleOrientation = handleOrientation;
    window.addEventListener('orientationchange', handleOrientation, false);
    StatusBar.backgroundColorByHexString("#000");
    
    //$rootScope.hideConfigure=false;
}

document.addEventListener('deviceready',onDeviceReady, false);


var MyCampusApp = {
    config : {
        tenant : "IIITD",
        serverUrl : "https://kryptos.kryptosmobile.com",
        tenantFolder : function(device, tenant) {
            if(device.platform == 'Android') {
                return "file://MyCampusMobile-" + tenant + "/";
            } else {
                return "file://MyCampusMobile-" + tenant + "/";
            }
        }
    } ,
    homeRoute : null,
    initMode: false,
    modalDialogDisplayed: false,
    homeScreenDisplayed : true,
    rootScope : null,
    currentPage : 1,
    //debugMode: window.tinyHippos != undefined,

    init : function(){
        MyCampusApp.initMode = true;
        document.addEventListener('deviceready', MyCampusApp.deviceReadyHandler, false);

    },

    isAppAuthorized : function(app, $rootScope) {
        var retval = false;
        if(!$rootScope.userroles) {
            return true;
        }
        if(app.appFeatureType == 'Public') {
            retval = true;
        }else {
            if(app.roles == null || app.roles.trim().length == 0) {
                retval = true;
            }else {
                var roles = app.roles.split(",");
                var added = false;
                for(var i=0;i<roles.length; i++) {
                    var role = roles[i];
                    if(added) {
                        break;
                    }
                    for(var j=0;j<$rootScope.userroles.length; j++) {
                        if($rootScope.userroles[j] == role) {
                            retval = true;
                            added = true;
                            break;
                        }
                    }
                }
            }
        }
        return retval;
    },
    fillRootScopeForHome : function($rootScope, $sce, tenant, $window, $location, $route, $http, $scope, $compile) {
        MyCampusApp.homeScreenDisplayed=true;
        MyCampusApp.rootScope = $rootScope;
        $.KNMonitor.initialize($rootScope);
        var storedMetadata =  $.jStorage.get( tenant + '-metadata'); //window.localStorage.getItem('metadata');
        //If Metadata doesn't exist in the local storage. Then this app is launched for the first time.
        //Lets pull the default metadata file.
        if(!storedMetadata) {
            $http.get("default-metadata.json").success(function(data){
                var tenantid = data.tenantid
                $.jStorage.set(tenantid + '-metadata', data);
                MyCampusApp.config.tenant = tenantid;
                $rootScope.tenant = tenantid;
                $.jStorage.set('tenant', tenantid);
                storedMetadata = data;

                if(window.device && data.pushconfig) {
                    MyCampusApp.activatePushNotification(tenantid, data.pushconfig);
                }
               // var message = '<div style="margin: 2px; vertical-align: middle; display: inline-block"><i class="icon-cog icon-spin icon-4x"></i><h3 style="color:white;">Initializing..</h3></div>';
                //var message = '<div style="margin:auto;position:fixed;left:0px;right:0px;vertical-align: middle; display: inline-block"><i class="icon-cog icon-spin icon-4x"></i><h3 style="color:white;">Initializing..</h3></div>';
                //$.blockUI({message : message});
                                                       $.blockUI({
                                                                 message: '<div id="floatingBarsG"><div class="blockG" id="rotateG_01"></div><div class="blockG" id="rotateG_02"></div><div class="blockG" id="rotateG_03"></div><div class="blockG" id="rotateG_04"></div><div class="blockG" id="rotateG_05"></div><div class="blockG" id="rotateG_06"></div><div class="blockG" id="rotateG_07"></div><div class="blockG" id="rotateG_08"></div></div><div></div>'
                                                                 });
                setTimeout(function() {
                    $.unblockUI();
                    if ($.jStorage.get('launchedonce')) {
                        $route.reload();
                    }else {
                        $route.reload();
                        $rootScope.$apply(function () {
                            //$location.path("/help");
                        $location.path("/home");
                        });
                    }
                },4000);
            }).error(function(data){
                });
        }

        //Store update bug fix start (Nick)
        else {
			if(!$rootScope.imageoptimized) {
                if(window.device && storedMetadata.pushconfig) {
                    
                    MyCampusApp.activatePushNotification(storedMetadata.tenantid, storedMetadata.pushconfig);
                    
                }
                
				$http.get("default-metadata.json").success(function(data){
                                                           $rootScope.imageoptimized = true;
                                                           if(data.version >= storedMetadata.version) {
                                                           var tenantid = data.tenantid
                                                           $.jStorage.set(tenantid + '-metadata', data);
                                                           MyCampusApp.config.tenant = tenantid;
                                                           $rootScope.tenant = tenantid;
                                                           $.jStorage.set('tenant', tenantid);
                                                           storedMetadata = data;
                                                           $rootScope.brandingUrl = storedMetadata.brandingurl + "?q=" + Math.random();
                                                           $rootScope.backgroundUrl = storedMetadata.backgroundurl + "?q=" + Math.random();
                                                           $.blockUI({
                                                                     message: '<div id="floatingBarsG"><div class="blockG" id="rotateG_01"></div><div class="blockG" id="rotateG_02"></div><div class="blockG" id="rotateG_03"></div><div class="blockG" id="rotateG_04"></div><div class="blockG" id="rotateG_05"></div><div class="blockG" id="rotateG_06"></div><div class="blockG" id="rotateG_07"></div><div class="blockG" id="rotateG_08"></div></div><div></div>'
                                                                     });
                                                           setTimeout(function() {
                                                                      $.unblockUI();
                                                                      $route.reload();
                                                                      },5000);
                                                           }
                                                           }).error(function(data){
                                                                    });
			}
		}
        //Store update bug fix end (Nick)

        if(storedMetadata) {
            //$location.path("/app/FoodDeals/FoodDeals");
            if($rootScope.loggedin) {
                if($rootScope.userroles) {
                   var authorizedApps = [];
                    $.each(storedMetadata.apps, function(index, val) {
                        if(MyCampusApp.isAppAuthorized(val, $rootScope)) {
                            authorizedApps.push(val);
                        }
                    });
                    $rootScope.apps = authorizedApps;
                } else {
                    $rootScope.apps = storedMetadata.apps;
                }
            } else {
                var publicApps = [];
                $.each(storedMetadata.apps, function(index, val) {
                    if(val.appFeatureType == 'Public') {
                        publicApps.push(val);
                    }
                });
                $rootScope.apps = publicApps;
            }

            //AK app grouping
            var appgroups = [];
            var findGroup = function(groupname) {
                var data = null;
                $.each(appgroups, function(index, val) {
                    if(val.appFeatureType == groupname) {
                        data = val;
                    }
                });
                return data;
            }
            $.each($rootScope.apps, function(index, val) {
                if(val.appFeatureType == 'Public') {
                    if(window.device) {
                        $rootScope.apps[index].logo=$rootScope.apps[index].logo + "?q=" + Math.random()
                    }
                }
                var group = findGroup(val.appFeatureType);
                if(group) {
                    group.apps.push(val);
                }else {
                    group = {appFeatureType : val.appFeatureType, apps : [val]};
                    appgroups.push(group);
                }
            });
            $rootScope.appgroups = appgroups;
            
            var UP = $.jStorage.get("UserProfile");

            if (UP) {
                //$rootScope.profileSynced = true;
                $rootScope.userProfileName = UP.username;
                $rootScope.useremail = UP.email;
                $rootScope.userProfilePic =UP.img;
            } else {
                $rootScope.userProfilePic ="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAABaCAAAAAAdwx7eAAAM3mlDQ1BJQ0MgUHJvZmlsZQAAWIWlVwdYU8kWnluS0BJ6lRI60gwoXUqkBpBeBFGJIZBACDEFAbEhiyu4dhHBsqKiKIsdgcWGBQtrB7sLuigo6+IqNixvEopYdt/7vnfzzb3/nXPOnDpnbgBQ5TAFAh4KAMjki4WBUfSEKQmJVNJdIAe0gTKwB8pMlkhAj4gIhSyAn8Vng2+uV+0AkT6v2UnX+pb+rxchhS1iwedxOHJTRKxMAJCJAJC6WQKhGAB5MzhvOlsskOIgiDUyYqJ8IU4CQE5pSFZ6GQWy+Wwhl0UNFDJzqYHMzEwm1dHekRohzErl8r5j9f97ZfIkI7rhUBJlRIfApz20vzCF6SfFrhDvZzH9o4fwk2xuXBjEPgCgJgLxpCiIgyGeKcmIpUNsC3FNqjAgFmIviG9yJEFSPAEATCuPExMPsSHEwfyZYeEQu0PMYYl8EyG2griSw2ZI8wRjhp3nihkxEEN92DNhVpSU3xoAfGIK289/cB5PzcgKkdpgAvFBUXa0/7DNeRzfsEFdeHs6MzgCYguIX7J5gVGD6xD0BOII6ZrwneDH54WFDvpFKGWLZP7Cd0K7mBMjzZkjAEQTsTAmatA2YkwqN4ABcQDEORxhUNSgv8SjAp6szmBMiO+FkqjYQR9JAWx+rHRNaV0sYAr9AwdjRWoCcQgTsEEWmAnvLMAHnYAKRIALsmUoDTBBJhxUaIEtHIGQiw+HEHKIQIaMQwi6RujDElIZAaRkgVTIyYNyw7NUkALlB+Wka2TBIX2Trtstm2MN6bOHw9dwO5DANw7ohXQORJNBh2wmB9qXCZ++cFYCaWkQj9YyKB8hs3XQBuqQ9T1DWrJktjBH5D7b5gvpfJAHZ0TDnuHaOA0fD4cHHop74jSZlBBy5AI72fxE2dyw1s+eS33rGdE6C9o62vvR8RqO4QkoJYbvPOghfyg+ImjNeyiTMST9lZ8r9CRWAkHpskjG9KoRK6gFwhlc1qXlff+StW+1232Rt/DRdSGrlJRv6gLqIlwlXCbcJ1wHVPj8g9BG6IboDuEu/N36blSyRmKQBkfWSAWwv8gNG3LyZFq+tfNzzgbX+WoFBBvhpMtWkVIz4eDKeEQj+ZNALIb3VJm03Ve5C/xab0t+kw6gti89fg5Qa1Qazn6Odhten3RNqSU/lb9CTyCYXpU/wBZ8pkrzwF4c9ioMFNjS9tJ6adtoNbQXtPufOWg3aH/S2mhbIOUptho7hB3BGrBGrBVQ4VsjdgJrkKEarAn+9v1Dhad9p8KlFcMaqmgpVTxUU6Nrf3Rk6aOiJeUfjnD6P9Tr6IqRZux/s2j0Ol92BPbnXUcxpThQSBRrihOFTkEoxvDnSPGByJRiQgmlaENqEMWS4kcZMxKP4VrnDWWY+8X+HrQ4AVKHK4Ev6y5MyCnlYA75+7WP1C+8lHrGHb2rEDLcVdxRPeF7vYj6xc6KhbJcMFsmL5Ltdr5MTvBF/YlkXQjOIFNlOfyObbgh7oAzYAcKB1ScjjvhPkN4sCsN9yVZpnBvSPXC/XBXaR/7oi+w/qv1o3cGm+hOtCT6Ey0/04l+xCBiAHw6SOeJ44jBELtJucTsHLH0kPfNEuQKuWkcMZUOv3LYVAafZW9LdaQ5wNNN+s00+CnwIlL2LYRotbIkwuzBOVx6IwAF+D2lAXThqWoKT2s7qNUFeMAz0x+ed+EgBuZ1OvSDA+0Wwsjmg4WgCJSAFWAtKAebwTZQDWrBfnAYNMEeewZcAJdBG7gDz5Mu8BT0gVdgAEEQEkJG1BFdxAgxR2wQR8QV8UL8kVAkCklAkpE0hI9IkHxkEVKCrELKkS1INbIPaUBOIOeQK8gtpBPpQf5G3qEYqoRqoAaoBToOdUXpaAgag05D09BZaB5aiC5Dy9BKtAatQ0+gF9A2tAN9ivZjAFPEtDBjzA5zxXyxcCwRS8WE2DysGCvFKrFa2ANasGtYB9aLvcWJuDpOxe1gFoPwWJyFz8Ln4UvxcnwnXoefwq/hnXgf/pFAJugTbAjuBAZhCiGNMJtQRCglVBEOEU7DDt1FeEUkErVgflxg3hKI6cQ5xKXEjcQ9xOPEK8SHxH4SiaRLsiF5ksJJTJKYVERaT6ohHSNdJXWR3sgpyhnJOcoFyCXK8eUK5Erldskdlbsq91huQF5F3lzeXT5cPkU+V365/Db5RvlL8l3yAwqqCpYKngoxCukKCxXKFGoVTivcVXihqKhoouimGKnIVVygWKa4V/GsYqfiWyU1JWslX6UkJYnSMqUdSseVbim9IJPJFmQfciJZTF5GriafJN8nv6GoU+wpDEoKZT6lglJHuUp5piyvbK5MV56unKdcqnxA+ZJyr4q8ioWKrwpTZZ5KhUqDyg2VflV1VQfVcNVM1aWqu1TPqXarkdQs1PzVUtQK1baqnVR7qI6pm6r7qrPUF6lvUz+t3qVB1LDUYGika5Ro/KJxUaNPU01zgmacZo5mheYRzQ4tTMtCi6HF01qutV+rXeudtoE2XZutvUS7Vvuq9mudMTo+OmydYp09Om0673Spuv66GbordQ/r3tPD9az1IvVm623SO63XO0ZjjMcY1pjiMfvH3NZH9a31o/Tn6G/Vb9XvNzA0CDQQGKw3OGnQa6hl6GOYbrjG8Khhj5G6kZcR12iN0TGjJ1RNKp3Ko5ZRT1H7jPWNg4wlxluMLxoPmFiaxJoUmOwxuWeqYOpqmmq6xrTZtM/MyGyyWb7ZbrPb5vLmruYc83XmLeavLSwt4i0WWxy26LbUsWRY5lnutrxrRbbytpplVWl1fSxxrOvYjLEbx162Rq2drDnWFdaXbFAbZxuuzUabK7YEWzdbvm2l7Q07JTu6XbbdbrtOey37UPsC+8P2z8aZjUsct3Jcy7iPNCcaD55udxzUHIIdChwaHf52tHZkOVY4Xh9PHh8wfv74+vHPJ9hMYE/YNOGmk7rTZKfFTs1OH5xdnIXOtc49LmYuyS4bXG64arhGuC51PetGcJvkNt+tye2tu7O72H2/+18edh4ZHrs8uidaTmRP3DbxoaeJJ9Nzi2eHF9Ur2etnrw5vY2+md6X3Ax9TnxSfKp/H9LH0dHoN/dkk2iThpEOTXvu6+871Pe6H+QX6Fftd9Ffzj/Uv978fYBKQFrA7oC/QKXBO4PEgQlBI0MqgGwwDBotRzegLdgmeG3wqRCkkOqQ85EGodagwtHEyOjl48urJd8PMw/hhh8NBOCN8dfi9CMuIWRG/RhIjIyIrIh9FOUTlR7VEq0fPiN4V/SpmUszymDuxVrGS2OY45bikuOq41/F+8aviO6aMmzJ3yoUEvQRuQn0iKTEusSqxf6r/1LVTu5KckoqS2qdZTsuZdm663nTe9CMzlGcwZxxIJiTHJ+9Kfs8MZ1Yy+2cyZm6Y2cfyZa1jPU3xSVmT0sP2ZK9iP071TF2V2p3mmbY6rYfjzSnl9HJ9ueXc5+lB6ZvTX2eEZ+zI+MSL5+3JlMtMzmzgq/Ez+KeyDLNysq4IbARFgo5Z7rPWzuoThgirRIhomqherAH/YLZKrCQ/SDqzvbIrst/Mjpt9IEc1h5/TmmuduyT3cV5A3vY5+BzWnOZ84/yF+Z1z6XO3zEPmzZzXPN90fuH8rgWBC3YuVFiYsfC3AlrBqoKXi+IXNRYaFC4ofPhD4A+7iyhFwqIbiz0Wb/4R/5H748Ul45esX/KxOKX4fAmtpLTk/VLW0vM/OfxU9tOnZanLLi53Xr5pBXEFf0X7Su+VO1eprspb9XD15NV1a6hrite8XDtj7bnSCaWb1ymsk6zrKAstq19vtn7F+vflnPK2ikkVezbob1iy4fXGlI1XN/lsqt1ssLlk87ufuT/f3BK4pa7SorJ0K3Fr9tZH2+K2tWx33V5dpVdVUvVhB39Hx86onaeqXaqrd+nvWr4b3S3Z3VOTVHP5F79f6mvtarfs0dpTshfslex9si95X/v+kP3NB1wP1B40P7jhkPqh4jqkLreu7zDncEd9Qv2VhuCG5kaPxkO/2v+6o8m4qeKI5pHlRxWOFh79dCzvWP9xwfHeE2knHjbPaL5zcsrJ66ciT108HXL67JmAMydb6C3HznqebTrnfq7hvOv5wxecL9S1OrUe+s3pt0MXnS/WXXK5VH/Z7XLjlYlXjl71vnrimt+1M9cZ1y+0hbVdaY9tv3kj6UbHzZSb3bd4t57fzr49cGcB/Igvvqdyr/S+/v3K38f+vqfDueNIp19n64PoB3cesh4+/UP0x/uuwkfkR6WPjR5Xdzt2N/UE9Fx+MvVJ11PB04Heoj9V/9zwzOrZwb98/mrtm9LX9Vz4/NPfS1/ovtjxcsLL5v6I/vuvMl8NvC5+o/tm51vXty3v4t89Hpj9nvS+7MPYD40fQz7e/ZT56dN/AC1d8Bx8dIG+AAAACXBIWXMAAAsTAAALEwEAmpwYAAABy2lUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNS40LjAiPgogICA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPgogICAgICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgICAgICAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyIKICAgICAgICAgICAgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIj4KICAgICAgICAgPHRpZmY6T3JpZW50YXRpb24+MTwvdGlmZjpPcmllbnRhdGlvbj4KICAgICAgICAgPHhtcDpDcmVhdG9yVG9vbD5BZG9iZSBJbWFnZVJlYWR5PC94bXA6Q3JlYXRvclRvb2w+CiAgICAgIDwvcmRmOkRlc2NyaXB0aW9uPgogICA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgqyI37xAAAIBElEQVRYCe1ZbW8cNRBee3fvrrkmTZO+Ja2gqVoVhIQQCIQKSPwF/nD5VPUjFAGiJIQmTZqQhKYvadq7fTHzzNhe793uXUPUD0g4Pa93PPPM4/Gs7d2qrWhyMYr68QuKQZs7AuFYMxmTjAjUCCy6SWRG3Y2Y0e1k6AZYC8E9zH4c00omQbcDe/xJ4O3QAbBtCg7dVIAqaI/Sb4Y2QYQZF2BKo2lKG+upzJuhtWPg+GqtTZERqk7SqCzFtSXfxrwR2gL6JIh1/uLZ4cvjzETJzNyFxZmyVITrMZVpykS15Qj6qwXmC1nruDzaffL3ICLmFI+ijOaur8zkVt0ytxePgcY4dIBMwCod7G3tHutUgyfIKZNnc19czqOSmoTQCj4C7eYP+PQzcfT44VPT0Q4AbCKlhsnnl+KkMCxuwx6BrlFWJh78sJ6mNVwBL4qz/YXbaSHhFnDrghVQ1aexQqYWIQ/v7Zw1oyZkZbQ+Pno0+Ezjeacfq0jtkSOfZhAJMnKa/2n1YGe2bECGbtKZX9tPWFE5Q8irEkJbZOokZXKQ7Kz1i0q13sJg1mlaKyNHzemF0CwDXxioSBe/x06t6Vqme6+JNmvXPFjlAJr9AxngIH34V9oSDTGOX7+kZ5//UKFILd0VNEsBC2QY6P2s6hXteq2K5xqKMHGo3BQ1nyGuWxRRF4eUAJPLsaSBUfRn86QycLycN1wlfLo8mhhqKNIDiT+uLG8HRCtZ5QV91CGKSul8MJqpoS63eU7asAVaFq4astJFUfM7hgvBCHZNR6x5FK4CCy7jD3jNNIrKLk0jJ4kEBoO2caFrAC1CxIMKdKdNYqS6mtUliB7WGlZjFo8OmWySpGmBB4AvPR4drePWg+1gKGGNJv1AlWpUyNdOlzasScWoMzxE2MCKhwoDIEhApGVxhTYcJDPToHUa0egEVyg5WEC7R4bhRUmUCfos74DQai4m6ViC6KeZ4ScHbY52GGsPL+D63JR5pG0TMMBC4UgG98QafdIvnXyviMFCMgUbMa6OlSFtuHIBIXAAc0hgQH9mJinFJRQbis6GfOqhLptLWEo4GrgiFXyRQHBN4vjFsAqX1wkaOttgLoRgFUHOK4iMWXqZkI+z3yRAgbzeLHu/PE0hwiiFECsAvXLHChB4FZPsb05L7OR4E08LCmMypG8m4oJqwPJwrK6ik920adQHOelwgC12ZWF8NNkZs8YAwaMbV3qOVP1q9OtSVhFPFQwtQZ8hDGiFApBMSRDW4uHiKMJ3lCgVHbB2jiQookR5nXYm5x6plz3JMAwSRWpu+qwhKYndUFjTdKetIbRg92l1RIG9s3YwVUI6cPYMtWSWjnRTyrzVsEaM4UwIGjA2QuyYWVBA4oUpK19kkgWy5SIRqVGx02jhoFV1L03Z0VU+v2Cng6hhNWGGLmV98jEmsfbIqrx85ThIIGEX1Eq9uD1jY8ZmMmavEUCTjFcZ4ONXdu50n7ceJyOVH336cZVDNiIWHgTVNo2EkRATyn+7bUBo4me7Px20nPtU3v96hV466MRqUEr+x028LFBoXKwxDDjmeLBPapfz80+3O3aKoRGW/JsVLAUSWQmyzQYbbLeoMvXQEm1VRF32NdpBXdnikjyDngwM+MZyQayDTjQD8irqj6OKpJhLxdAqMIhT5pvaNLJTRscEkJ6Jei6VnJVDotcyHylGYn1+gxMEtz2IAXRZzeHQYaRtfzQdq0l85JWQ7nnADoFZu5tRZLrvts2iQahkbEEEHSdcawEJO6y7tBp22EtEr9h7HxY7YH/fCs2Gysxef92koo8+uuYxvE+W+BA4OxLwM+jVbEPdufbG6VR96s3yVxKMSsYtj0t3bEbexiiIQJkz33YbHvbyyzPVM+7hQ2ALXReJppddfH+MtsrPLUZuEryi92Ab4dLmdjjukvOQUds/HvRG122jh9/3b19t4B0OPoA2dMqyByxHwOjs3qNz47xUufHq4Ds52odozo6v4RQ1fUPY3L0wjkxT3ru4uzn6oIoT78pNI/zwcx5OqFHZA90wiaRbFvpBRuFGEEGJAbnyTOqssfbyYLiij1VrG71AUHURVm9jjWnjk1bNymnVP/04KVzQh5D48H4bMrDvH8asZp3TBTuB+KEbrNcjvEChNDqJy/27r1r2GOJg0ld398uEVgK251qCAURqBRuYbF2oI52qo4Odnb2ibXHi4alhfGlp6ULfZDRC2sCoSE3Q5M5B4zCr8T5FjTgtn66vHjIjF6LGqyqzXC3cvHFeE7jDlZAE0Nh0sefSl814+GR1Y9BN6JtjI2AopL05H3Tfu7XUzXOmzVXIGksTCkfi8epu1KMJCiEmtJUq3kRXbl09a3LExW7yZCABQUojEok5XP/jZdqhDx0nKdoMs9kbKxQX/qqIkFTQNKMUicH++hZF4q0JV+6VyoedaysXO2VOrAJoIh/H5nj7z4Ooq09GuELX5SC6sLI8o4pCDicUEAJOojeH20+Omr5xVrZTWyrKsv7V5fO9qMCiqLZNlBaE+8LQ5v22U9fmBXFRc0vLC3FO07el9N7qXp4kJ5y6NnRt8jy5dPMyfT3fNr8+VO5pbdM/kZzOuJn54MMoUQ8e9vn/Fk5kPkmZwtqJfs4+SbZW+6eN8LgfE/VXF/WjYAsb1/n3kmRD4034HRSjCoJ+B8AEqaZ86z2N1+r1/zQojbZKZ//BgPiTW+OYTiU07y5Dcvc17VQEG41pA39n5X/okdD+AyvykGq5t4FmAAAAAElFTkSuQmCC";
                $rootScope.userProfileName = "";
                $rootScope.userRole="Role";
                $rootScope.$apply();
            }
            try{
                
                
                
                
                
                var data={tenant:'IIITD',max:20,offset:0};
                //$.jStorage.set("pushOldCounts",0);
                var xhr=$.ajax({
                               url:"https://kryptos.kryptosmobile.com/gateway/CEAI/kryptospushlog",
                               contentType:'application/json',
                               type:"POST",
                               headers: { 'licenseKey': 'UjpIaWVDVzpTOjE0NDk0ODk2MzUxMzE6VTpha0BjYW1wdXNlYWkub3JnOlQ6NDpQOjI4Ng==' },
                               data:JSON.stringify(data),
                               success:function(data){
                               
                               $rootScope.pushCounts=data.paging.total;
                               $rootScope.pushData=data;
                               
                               var oldCounts=$.jStorage.get("pushOldCounts");
                               
                               if(oldCounts == null || oldCounts == undefined){
                               //var oldCounts = $rootScope.pushCounts;
                               $rootScope.NewPush=$rootScope.pushCounts;
                               //$.jStorage.set("pushOldCounts",$rootScope.pushCounts);
                               }
                               else{
                               var oldCounts=$.jStorage.get("pushOldCounts");
                               $rootScope.NewPush=$rootScope.pushCounts-oldCounts;
                               //$.jStorage.set("pushOldCounts",$rootScope.pushCounts);
                               }

                               if($rootScope.NewPush <= 0){
                               $rootScope.ShowPushCounts=false;
                               }else{
                               $rootScope.ShowPushCounts=true;
                               }
                               //alert($rootScope.NewPush);
                               $rootScope.notificationCounts=$rootScope.NewPush;
                               },
                               error:function(err){
                               }
                               });
            }catch(e){
                alert(e)
            }
            
            
            $rootScope.appDisplayName = storedMetadata.appDisplayName;
            $rootScope.metadata = storedMetadata;
            $rootScope.middlewareServerUrl = storedMetadata.middlewareServerUrl;
            $rootScope.customStyle = $sce.trustAs($sce.CSS, storedMetadata.customStyle);

            $('#customstyle').html(storedMetadata.customStyle);
            try {
                var data1 = $compile($(storedMetadata.homeScreenTemplate))($scope);
                $("#homecontent").html(data1);
                //$location.path("/app/FoodDeals/FoodDeals");
            }catch(exce) {
                //Ignore..
            }
            window.eval(storedMetadata.authFunction);
        }

        if( $.jStorage.get('serverUrl') ) { // window.localStorage.getItem('serverUrl') ) {
            $rootScope.serverUrl =  $.jStorage.get('serverUrl');  //window.localStorage.getItem('serverUrl');
        }
        if( $.jStorage.get('tenant') ) {//window.localStorage.getItem('tenant') ) {
            $rootScope.tenant = $.jStorage.get('tenant'); //window.localStorage.getItem('tenant');
        }

        $rootScope.setRoute = function (route) {
            $location.path(route);
        };
        $rootScope.logout = function() {
            var logoutcleanup = function(buttonIndex) {
                if(buttonIndex == 1) {
                    $rootScope.ticket = null;
                    $rootScope.loggedin = false;
                    $.jStorage.deleteKey('username');
                    $.jStorage.deleteKey('password');
                    $.jStorage.deleteKey('ticket');
                    document.removeEventListener("resume",function(){});
                    if( angular.isFunction( $rootScope["logoutcb"] ) ) {
                        $rootScope["logoutcb"]();
                    }
                    $route.reload();
                }
            };
            if(window.device) {
                navigator.notification.confirm(
                    'Are you sure you want to logout?', // message
                    logoutcleanup,            // callback to invoke with index of button pressed
                    'Just Confirming',           // title
                    ['Yes','No']         // buttonLabels
                );
            }else {
                apprise("Are you sure you want to logout?", {'verify':true, 'textYes':"Yes", 'textNo':"No"}, function(r) {
                    if(r) {
                        logoutcleanup(1);
                    }
                    else {
                        MyCampusApp.modalDialogDisplayed = false;
                    }
                });
            }

        };
        $rootScope.loginlogout = function() {
            if($rootScope.loggedin) {
                $rootScope.logout();
            }else {
                $rootScope.setRoute("/login");
            }
        };
        $rootScope.back = function() {
            $window.history.back();
        };
        $rootScope.showlogin=true;
        if(window.device) {
            if($rootScope.metadata) {
                if(!$rootScope.brandingUrl) {
                    $rootScope.brandingUrl = $rootScope.metadata.brandingurl + "?q=" + Math.random();
                }
                if(!$rootScope.backgroundUrl) {
                    $rootScope.backgroundUrl = $rootScope.metadata.backgroundurl + "?q=" + Math.random();
                }
            }
        }else {
            if(!$rootScope.brandingUrl) {
                $rootScope.brandingUrl = "/metaData/branding/q=" + Math.random();
            }
            if(!$rootScope.backgroundUrl) {
                $rootScope.backgroundUrl = "/metaData/background/q=" + Math.random();
            }
        }

        $rootScope.leftswipe = function() {
            $rootScope.stage.next();
        }
        $rootScope.rightswipe = function() {
            $rootScope.stage.previous();
        }
    },

    deviceReadyHandler : function() {
        document.addEventListener("backbutton", MyCampusApp.backButtonHandler, true);
        document.addEventListener('pause', MyCampusApp.pauseHandler, false);
        document.addEventListener('resume', MyCampusApp.resumeHandler, false);
        document.addEventListener('online', MyCampusApp.onlineHandler, false);
        document.addEventListener('offline', MyCampusApp.offlineHandler, false);
    },
    backButtonHandler: function() {
        $.unblockUI();
        if(MyCampusApp.homeScreenDisplayed) {
            var onConfirm = function(buttonIndex) {
                if(buttonIndex == 1) {
                    navigator.app.exitApp();
                }
            };
            if(window.device) {
                navigator.notification.confirm(
                    'Are you sure you want to exit?', // message
                    onConfirm,            // callback to invoke with index of button pressed
                    'Just Confirming',           // title
                    ['Yes','No']         // buttonLabels
                );
            }else {
                apprise("Are you sure you want to exit?", {'verify':true, 'textYes':"Yes", 'textNo':"No"}, function(r) {
                    if(r) {
                        navigator.app.exitApp();
                    }
                    else MyCampusApp.modalDialogDisplayed = false;
                });
            }
        }else {
            navigator.app.backHistory();
        }
    },

    pauseHandler: function(){

    },

    resumeHandler: function(){
        try {
            $.unblockUI();
			if(MyCampusApp.rootScope) {
				MyCampusApp.rootScope.updateCheck();
			}
        }catch(e) {
            //alert("Exception in Resume handler : " + e);
        }
    },

    onlineHandler: function(){
        try {
            //alert("Online Called");
            var rootScope = MyCampusApp.rootScope;
            rootScope.$broadcast("onOnline", "Device is Online");
        }catch(e) {
            //alert("Exception in Online handler : " + e);
        }
    },

    offlineHandler: function(){
        try {
            //alert("Online Called");
            var rootScope = MyCampusApp.rootScope;
            rootScope.$broadcast("onOffline", "Device is Offline");
        }catch(e) {
            //alert("Exception in Online handler : " + e);
        }
    },

    updateAppLogos : function(tenant) {

        var processLogosDir = function(logosDir) {
            var reader = logosDir.createReader();
            reader.readEntries(function(entries){
                for (var i=0; i<entries.length; i++) {
                    if(entries[i].name == 'branding') {
                    }else {
                    }
                }
            },null);

        };

        var onFileSystemSuccess =  function(fileSystem) {
            fileSystem.root.getDirectory("MyCampusMobile-" + tenant ,{create:true},processLogosDir,null);
        };

        if(window.LocalFileSystem) {
            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, onFileSystemSuccess, null);
        }
        //fileSystem.root.getDirectory("Android/data/org.campuseai." + tenant + ".appLogos",{create:true},gotDir,onError);
    },

    checkAndUpdateMetadata : function(tenant, url, $http, currentVersion,  $route, $rootScope, $scope, $sce, logosDirPath, $compile, silent) {
        $http.post(url + "/metagate/updatecheck/" + tenant + "?callback=JSON_CALLBACK", {device: window.device}).
            success(function(data) {
                if(data.version != currentVersion) {
                    var onConfirm = function(buttonIndex) {
                        if(buttonIndex == 1) {
                            MyCampusApp.updateMetadata(tenant, url, $http, data, $route, $rootScope, $scope, $sce, logosDirPath, $compile);
                        }
                        $rootScope.umalert = false;
                    };
                    if(!silent) {
						if(window.device) {
							if(!$rootScope.umalert) {
								$rootScope.umalert = true;
								navigator.notification.confirm(
									'App Updates available. Update?', // message
									onConfirm,            // callback to invoke with index of button pressed
									'Update Manager',           // title
									['Yes','No']         // buttonLabels
								);
							}
						}else {
							apprise("App Updates available. Update? ", {'verify':true, 'textYes':"Yes", 'textNo':"No"}, function(r) {
								if(r) {
									//navigator.app.exitApp();
									MyCampusApp.updateMetadata(tenant, url, $http, data, $route, $rootScope, $scope, $sce, logosDirPath, $compile);
								} else MyCampusApp.modalDialogDisplayed = false;
							});
						}
					}else {
						onConfirm(1);
					}
                }
            });
    },

    updateMetadata : function(tenant, url, $http, data, $route, $rootScope, $scope, $sce, logosDirPath, $compile) {
        $.blockUI({
                  message: '<div id="floatingBarsG"><div class="blockG" id="rotateG_01"></div><div class="blockG" id="rotateG_02"></div><div class="blockG" id="rotateG_03"></div><div class="blockG" id="rotateG_04"></div><div class="blockG" id="rotateG_05"></div><div class="blockG" id="rotateG_06"></div><div class="blockG" id="rotateG_07"></div><div class="blockG" id="rotateG_08"></div></div><div></div>'
                  });
        $http.post(url + "/metagate/metadata/" + tenant + "?callback=JSON_CALLBACK", {source: data.source, id : data.id, device: window.device}).
            success(function(data) {
                if(window.device && data.pushconfig) {
                    MyCampusApp.activatePushNotification(tenant, data.pushconfig);
                }
                MyCampusApp.refreshMetdata(data, $rootScope, $scope, $sce, tenant, url, logosDirPath, $route, $compile);
                //$.jStorage.set(tenant + '-metadata', data);
               // $route.reload();
            }).
            error(function(date) {
                $.unblockUI();
            });
    },

    refreshMetdata : function(data, $rootScope, $scope, $sce, tenant, baseUrl, logosDirPath, $route, $compile) {

        //window.localStorage.setItem('metadata', data);
        var message = '<div style="margin: 2px; vertical-align: middle; display: inline-block"><i class="icon-cog icon-spin icon-4x"></i><h3 style="color:white;">Initializing</h3></div>';
        //$.blockUI({message : message});
        //$.blockUI();
        if(window.device) {
            data.brandingurl = logosDirPath + "/" +  "branding";
            data.backgroundurl = logosDirPath + "/" + "background";
        }
        var allIcons, allScreens, dock, dockIcons, icon, stage, _i, _len, _results;
        allIcons = [];
        dockIcons = [];
        $.each(data.apps, function(index, val) {
            if(window.device) {
                data.apps[index].logo = logosDirPath + "/" + val.name;
            }else {
                data.apps[index].logo = "/metaData/appLogo/" + val.id;
            }

            if(MyCampusApp.isAppAuthorized(val, $rootScope)) {
                if($rootScope.loggedin || val.appFeatureType == 'Public') {
                    if(val.showInHome) {
                        allIcons.push(new Icon(val.name, val.displayname, "#app/" + val.name + "/" + val.name, val.logo));
                    }
                    if(val.showInDock) {
                        dockIcons.push(new DockIcon(val.name, val.displayname, "#app/" + val.name + "/" + val.name, val.logo));
                    }
                }else if(val.appFeatureType == 'Public') {
                    if(val.showInHome) {
                        allIcons.push(new Icon(val.name, val.displayname, "#app/" + val.name + "/" + val.name, val.logo));
                    }
                    if(val.showInDock) {
                        dockIcons.push(new DockIcon(val.name, val.displayname, "#app/" + val.name + "/" + val.name, val.logo));
                    }
                }
            }
        });
        $rootScope.customStyle = $sce.trustAs($sce.CSS, data.customStyle);
        try {
            var data1 = $compile($(data.homeScreenTemplate))($scope);
            $("#homecontent").html(data1);
            //alert("aa");
        }catch(exce) {
            //Ignore..
        }

        $('#customstyle').html(data.customStyle);


        //allIcons = [new Icon('Photos', 'Photo Gallery'), new Icon('Maps', 'Google Maps'), new Icon('Chuzzle', 'Chuzzle'), new Icon('Safari', 'Safari'), new Icon('Weather', 'Weather'), new Icon('nes', 'NES Emulator'), new Icon('Calendar', 'Calendar'), new Icon('Clock', 'Clock'), new Icon('BossPrefs', 'Boss Prefs'), new Icon('Chess', 'Chess'), new Icon('Mail', 'Mail'), new Icon('Phone', 'Phone'), new Icon('SMS', 'SMS Center'), new Icon('Camera', 'Camera'), new Icon('iPod', 'iPod'), new Icon('Calculator', 'Calculator'), new Icon('Music', 'Music'), new Icon('Poof', 'Poof'), new Icon('Settings', 'Settings'), new Icon('YouTube', 'Youtube'), new Icon('psx4all', 'PSx4All'), new Icon('VideoRecorder', 'Record Video'), new Icon('Installer', 'Installer'), new Icon('Notes', 'Notes'), new Icon('RagingThunder', 'RagingThunder'), new Icon('Stocks', 'Stocks'), new Icon('genesis4iphone', 'Genesis'), new Icon('snes4iphone', 'SNES Emulator'), new Icon('Calendar', 'Calendar'), new Icon('Clock', 'Clock'), new Icon('Photos', 'Photo Gallery'), new Icon('Maps', 'Google Maps')];
        //dockIcons = [new DockIcon('Camera', 'Camera'), new DockIcon('iPod', 'iPod'), new DockIcon('Calculator', 'Calculator')];
        var docWidth  = $(document).width();
        var winWidth = $(window).width();
        if(docWidth != winWidth) {
            docWidth = docWidth - 15;
        }
        if(docWidth >= 768) {

        }

        Stage.prototype.screenWidth = docWidth; //$(window).width() + 16;
        allScreens = $('#allScreens');
        allScreens.html("");
        //allScreens.Touchable();
        if($.isNumeric(data.iconWidth)) {
            stage = new Stage(allIcons, data.iconWidth);
        }else {
            stage = new Stage(allIcons, 64);
        }

        stage.addScreensTo(allScreens);
        stage.addIndicatorsTo('#indicators');
        $rootScope.stage=stage;
        dock = $('#dock');
        dock.html("");
        _results = [];
        for (_i = 0, _len = dockIcons.length; _i < _len; _i++) {
            icon = dockIcons[_i];
            _results.push(dock.append(icon.markup));
        }


        /* Commenting for icon issue during update manager - Start (Nick)
        var homedata = $("#homedata");
        homedata.html("");
        var iconwidth = 64;
        var sampleicon = $('#sampleicon');
        var calculated = sampleicon.outerWidth(true);
        if(calculated != 0) {
            iconwidth = sampleicon.outerWidth(true) + 24;
        }
        var width = $(window).width() > 780 ? $(window).width() - 280 : $(window).width();
        var cols =  Math.floor(width / iconwidth);
        var rows = Math.floor(($(window).height() - 200) / iconwidth);

        for (_i = 0, _len = allIcons.length; _i < _len; _i++) {
            icon = allIcons[_i];
            var markup = '<li><a href="' + icon.url + '"><img src="' + icon.logourl + '" class="icon"></img></a>' +
                '<div class="campuseai-Info text-center" style="width:' + calculated + 'px;overflow:hidden;text-overflow: ellipsis;">'
                + icon.title + '</div></li>';
            homedata.append(markup);
        }

        //AK added for promptumenu
        $("#homedata").promptumenu({
            width:(width - 24),
            height:($(window).height() - 200),
            rows: rows,
            columns: cols,
            direction: 'horizontal',
            pages: true
        });
        //End AK

         Commenting for icon issue during update manager - end (Nick)*/

        /*
         if(window.device) {
         $rootScope.brandingUrl = MyCampusApp.config.tenantFolder(window.device, tenant) + "branding?q=" + Math.random();
         $rootScope.backgroundUrl = MyCampusApp.config.tenantFolder(window.device, tenant) + "background?q=" + Math.random();
         }
         */

        /*
         $.each(data.apps, function(index, val) {
         if(window.device) {
         //data.apps[index].logo = "file://Android/data/org.campuseai." + tenant + ".appLogos/" + val.name;
         data.apps[index].logo = MyCampusApp.config.tenantFolder(window.device, tenant) + val.name;
         }else {
         data.apps[index].logo = "/comet/metaData/appLogo/" + val.id;
         }
         });
         */

        $.jStorage.set(tenant + '-metadata', data);

        if($rootScope.loggedin) {
            $rootScope.apps = data.apps;
        } else {
            var publicApps = [];
            $.each(data.apps, function(index, val) {
                if(val.appFeatureType == 'Public') {
                    publicApps.push(val);
                }
            });
            $rootScope.apps = publicApps;
        }

        //AK app grouping
        var appgroups = [];
        var findGroup = function(groupname) {
            var data = null;
            $.each(appgroups, function(index, val) {
                if(val.appFeatureType == groupname) {
                    data = val;
                }
            });
            return data;
        }
        $.each($rootScope.apps, function(index, val) {
            var group = findGroup(val.appFeatureType);
            if(group) {
                group.apps.push(val);
            }else {
                group = {appFeatureType : val.appFeatureType, apps : [val]};
                appgroups.push(group);
            }
        });
        $rootScope.appgroups = appgroups;
        console.log("App groups " + appgroups);
        $rootScope.appDisplayName = data.appDisplayName;
        $rootScope.metadata = data;
        $rootScope.middlewareServerUrl = data.middlewareServerUrl;
        $rootScope.customStyle = $sce.trustAs($sce.CSS, data.customStyle);
            $('#customstyle').html(data.customStyle);
        window.eval(data.authFunction);
        console.log($scope.apps);
        console.log($rootScope.appDisplayName);
        var downcounter = 0;
        var onError = function(e){
            downcounter--;
            $.unblockUI();
            console.log("ERROR");
            console.log(JSON.stringify(e));
            alert ("Error inside onError : " + JSON.stringify(e));

        };

        var onFileSystemSuccess = function(fileSystem) {

            var  gotDir = function(d){
                var message = '<div style="margin: 2px; vertical-align: middle; display: inline-block"><i class="icon-cog icon-spin icon-4x"></i><h3>Installing Updates.!</h3></div>';
                //$.blockUI({message : message});
                //$.blockUI();

                console.log("got dir");
                var DATADIR = d;
                var reader = DATADIR.createReader();

                var ft = new FileTransfer();

                //var DATADIR = "Android/data/org.campuseai." + tenant + "/appLogos";

                //console.log("downloading crap to " + dlPath);
                var url = baseUrl + "/metaData/branding/" + tenant + "?q=" + Math.random();
                var dlPath = DATADIR.toURL() + "/branding";
                downcounter++;
                ft.download(url, dlPath, function(){
                    downcounter--;
                    /*message = '<div style="margin: 2px; vertical-align: middle; display: inline-block"><i class="icon-cog icon-spin icon-4x"></i><h3>Branding completed.!</h3></div>';
                     $.blockUI({message : message});
                     setTimeout(function() {
                     $.unblockUI();
                     },1000);*/

                    //renderPicture(dlPath);
                    //$('#branding').attr('src', dlPath);
                }, onError, true);

                url = baseUrl + "/metaData/background/" + tenant + "?q=" + Math.random();
                dlPath = DATADIR.toURL() + "/background";
                /*message = '<div style="margin: 2px; vertical-align: middle; display: inline-block"><i class="icon-cog icon-spin icon-4x"></i><h3>Downloading background.!</h3></div>';
                 $.blockUI({message : message});*/
                downcounter++;
                ft.download(url, dlPath, function(){
                    downcounter--;
                    /*message = '<div style="margin: 2px; vertical-align: middle; display: inline-block"><i class="icon-cog icon-spin icon-4x"></i><h3>Background completed.!</h3></div>';
                     $.blockUI({message : message});
                     setTimeout(function() {
                     $.unblockUI();
                     },1000);*/
                    //renderPicture(dlPath);
                    //$('#branding').attr('src', dlPath);
                }, onError, true);

                $.each(data.apps, function(index, val) {
                    url = baseUrl + "/metaData/appLogo/" + val.id
                    dlPath = DATADIR.toURL() + "/" + val.name;
                    /*message = '<div style="margin: 2px; vertical-align: middle; display: inline-block"><i class="icon-cog icon-spin icon-4x"></i><h3>Downloading ' + val.id + ' icons.!</h3></div>';
                     $.blockUI({message : message});*/
                    downcounter++;
                    ft = new FileTransfer();
                    ft.download(url, dlPath, function(){
                        downcounter--;
                        /*message = '<div style="margin: 2px; vertical-align: middle; display: inline-block"><i class="icon-cog icon-spin icon-4x"></i><h3>' + val.id + ' icon Downloaded.!</h3></div>';
                         $.blockUI({message : message});
                         setTimeout(function() {
                         $.unblockUI();
                         },1000);*/
                        //renderPicture(dlPath);
                        //$('#logo-' + val.name).attr('src', dlPath);
                    }, onError, true);

                });
                (function loop(){
                    setTimeout(function() {
						//alert ("Inside loop.." + downcounter);
                        if(downcounter == 0) {
                            //$route.reload();
                            //$rootScope.broadcast("");
                            //alert ("Inside downcounter 0.." + downcounter);
                            try{
                            $rootScope.$broadcast("onDownloadComplete", "Download Completed");
                            //alert ("Broadcast called..");
                            setTimeout(function() {
                                $.unblockUI();
                            },2000);
						}catch(exce) {
							//alert ("Exception .." + exce);
						}

                        } else {
                            loop();
                        }
                    },2000);
                })();
//                    setTimeout(function() {
//                        $route.reload();
//                    },1000);

                /*setTimeout(function() {
                 MyCampusApp.updateAppLogos(tenant);
                 }, 5000);*/
            };
            fileSystem.root.getDirectory("MyCampusMobile-" + tenant ,{create:true},gotDir,onError);
        };

        if(window.LocalFileSystem) {
            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, onFileSystemSuccess, null);
        }
        setTimeout(function() {
            if(!window.device) {
                $.unblockUI();
            }
        }, 2000);
        //$scope.$apply();
    },

    invokeService : function($rootScope, $http, endpoint, method, data, successCB, errorCB ) {
        var serviceurl = $rootScope.middlewareServerUrl + endpoint;
        var isLogin = endpoint.indexOf("login") != -1;
        if($rootScope.ticket) {
            if(serviceurl.indexOf("?") != -1) {
                serviceurl=serviceurl + "&ticket=" + $rootScope.ticket;
            }else {
                serviceurl=serviceurl + "?ticket=" + $rootScope.ticket;
            }
        }
        var url = "";
        var proxyMethod = method;
        var proxyData = data;
        if(window.device) {
            url = serviceurl;
        }else {
            url = "/websimulator/json?url=" + serviceurl;
            proxyMethod = "POST";
            proxyData =  {method:method, body:data}
        }
        $http({method:proxyMethod, url: url, data: proxyData}).
            success(function(data, status, headers, config) {
                successCB(data, status, headers, config);
            }).
            error(function(data, status, headers, config) {

                if(!isLogin) {
                    alert ("Ticket might have expired.. silent authenticating");
                    MyCampusApp.silentAuth($rootScope, $http, function(data, status, headers, config) {
                        MyCampusApp.invokeService($rootScope, $http, endpoint, method, data, successCB, errorCB);
                    }, function() {

                    });
                }
                errorCB(data, status, headers, config);
            });
    },

    silentAuth : function($rootScope, $http, successCB, errorCB) {
        var username = $.jStorage.get('username');
        var password = $.jStorage.get('password');
        var data = "username=" + username + "&password=" + password;
        MyCampusApp.invokeService($rootScope, $http, "services/authenticate/login", "POST", data,
            function(data, status, headers, config) {
                //console.log("Success Data : " + data);
                //alert ("Success : " + data);
                if(data.error) {
                    apprise(data.error, {'verify':false, 'textYes':"Ok"}, function(r) {
                        $rootScope.ticket = null;
                        $rootScope.loggedin = false;
                        $.jStorage.deleteKey('username');
                        $.jStorage.deleteKey('password');
                        $.jStorage.deleteKey('ticket');
                    });
                }else {
                    var ticket = data.ticket;
                    $rootScope.ticket = ticket;
                    $rootScope.loggedin = true;
                    $.jStorage.set('username', username);
                    $.jStorage.set('password', password);
                    $.jStorage.set('ticket', ticket);
                    successCB(data, status, headers, config);
                }
            }, function(data, status, headers, config) {
                console.log("Error Data : " + data);
                alert ("Error : " + data);
            });
    },

    activatePushNotification : function(tenantId, pushconfig) {
        try {
            var appId = pushconfig.ApplicationId;
            var clientKey = pushconfig.ClientKey;
            parsePlugin.initialize(appId, clientKey, function() {
                //alert('Parse initialize success');
            }, function(e) {
                //alert('Parse initialize error');
            });

            parsePlugin.getInstallationId(function(id) {
                //alert(id);
            }, function(e) {
                //alert('error');
            });

            parsePlugin.getSubscriptions(function(subscriptions) {
                //alert(subscriptions);
            }, function(e) {
                //alert('error');
            });

            parsePlugin.subscribe(tenantId, function() {
                //alert('OK');
            }, function(e) {
                //alert('error');
            });

            /*parsePlugin.unsubscribe('SampleChannel', function(msg) {
             alert('OK');
             }, function(e) {
             alert('error');
             });*/
        }catch(e) {

        }

    },

    logPageAccess : function(tenant, url, $http, appid, appname, pageid) {
        try {
            var mydevice;
            if(window.device) {
                mydevice = window.device;
            }else {
                mydevice = {
                    model : "NA",
                    cordova : "NA",
                    platform : "NA",
                    uuid : "Emulator",
                    version : "NA",
                    name : "NA",
                }
            }
            $http.post(url + "/analytics/logpageaccess/" + tenant + "?callback=JSON_CALLBACK", {
                activity : {
                    "appid" : appid,
                    "appname"  : appname,
                    "pageid" : pageid
                }, device: mydevice}).
                success(function(data) {
                    //Ignore
                });
        }catch(e) {
            //Ignore
        }
    }

};


var DockIcon, Icon, Screen, Stage,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) {
        for (var key in parent) {
            if (__hasProp.call(parent, key))
                child[key] = parent[key];
        }
        function ctor() {
            this.constructor = child;
        }
        ctor.prototype = parent.prototype;
        child.prototype = new ctor();
        child.__super__ = parent.prototype; return child;
    };

Icon = (function() {

    function Icon(id, title, url, logourl) {
        this.id = id;
        this.title = title;
        this.url = url;
        this.logourl=logourl;
        this.markup = "<a href='" + this.url + "' class='icon' style='background-image:url(" + this.logourl + ");background-size:100% 100%;'  title='" + this.title + "'></a>";
    }

    return Icon;

})();

DockIcon = (function(_super) {

    __extends(DockIcon, _super);

    function DockIcon(id, title, url, logourl) {
        DockIcon.__super__.constructor.call(this, id, title, url, logourl);
        this.markup = this.markup.replace("class='icon'", "class='dockicon'");
    }

    return DockIcon;

})(Icon);

Screen = (function() {

    function Screen(icons) {
        if (icons == null) {
            icons = [];
        }
        this.icons = icons;
    }

    Screen.prototype.attachIcons = function(icons) {
        if (icons == null) {
            icons = [];
        }
        return Array.prototype.push.apply(this.icons, icons);
    };

    Screen.prototype.generate = function() {
        var icon, markup, _i, _len, _ref;
        markup = [];
        _ref = this.icons;
        var width = $(window).width();
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            icon = _ref[_i];
            markup.push(icon.markup);
        }
        return "<div class='screen' style='width:" + width + "px; !important;'>" + (markup.join('')) + "</div>";
    };

    return Screen;

})();

Stage = (function() {

    Stage.prototype.screenWidth = 279;

    function Stage(icons, iconwidth) {
        //alert ($(window).width());

        if(!iconwidth) {
            iconwidth = 64;
        }
        var sampleicon = $('#sampleicon');
        var calculated = sampleicon.outerWidth(true);
        if(calculated != 0) {
            iconwidth = sampleicon.outerWidth(true) + 24;
        }
        var cols =  Math.floor(($(window).width() - 24) / iconwidth);
        var remainder = ($(window).width() - 24) % iconwidth;
        var rows = Math.floor(($(window).height() - 160) / iconwidth);
        var maskHeight = (iconwidth * rows) + 24;
        var maskStyle = "width:" + ($(window).width() - 24) + "px !important;height:" + maskHeight + "px !important;";
        $('#mask').attr("style",maskStyle);
        //alert ("Cols " + cols);
        //alert ("Rows " + rows);
        var noOfIconsAllwedInAScreen = cols * rows;
        var i, num, s;
        this.currentScreen = 0;
        this.screens = [];
        num = Math.ceil(icons.length / noOfIconsAllwedInAScreen);
        i = 0;
        while (num--) {
            s = new Screen(icons.slice(i, i + noOfIconsAllwedInAScreen));
            this.screens.push(s);
            i += noOfIconsAllwedInAScreen;
        }
    }

    Stage.prototype.addScreensTo = function(element) {
        var screen, _i, _len, _ref, _results;
        this.element = $(element);
        this.element.width((this.screens.length * this.screenWidth) + 15);
        _ref = this.screens;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            screen = _ref[_i];
            _results.push(this.element.append(screen.generate()));
        }
        return _results;
    };

    Stage.prototype.addIndicatorsTo = function(elem) {
        var screen, _i, _len, _ref;
        this.ul = $(elem);
        _ref = this.screens;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            screen = _ref[_i];
            this.ul.append('<li>');
        }
        return this.ul.find('li:first').addClass('active');
    };

    Stage.prototype.goTo = function(screenNum) {
        var from, to, _ref, _ref1,
            _this = this;
        if (this.element.is(':animated')) {
            return false;
        }
        if (this.currentScreen === screenNum) {
            _ref = ['+=0', '-=0'], from = _ref[0], to = _ref[1];
            if (this.currentScreen !== 0) {
                _ref1 = [to, from], from = _ref1[0], to = _ref1[1];
            }
            return this.element.animate({
                marginLeft: from
            }, 150).animate({
                    marginLeft: to
                }, 150);
        } else {
            this.element.animate({
                marginLeft: -screenNum * (this.screenWidth)
            }, function() {
                return _this.currentScreen = screenNum;
            });
            return this.ul.find('li').removeClass('active').eq(screenNum).addClass('active');
        }
    };

    Stage.prototype.next = function() {
        var toShow;
        toShow = this.currentScreen + 1;
        if (toShow === this.screens.length) {
            toShow = this.screens.length - 1;
        }
        return this.goTo(toShow);
    };

    Stage.prototype.previous = function() {
        var toShow;
        toShow = this.currentScreen - 1;
        if (toShow === -1) {
            toShow = 0;
        }
        return this.goTo(toShow);
    };

    return Stage;

})();

/** Kryptos Network Monitor **/
(function(){
    var onlineCallbacks = {};
    var offlineCallbacks = {};
    var initialized = false;

    /** Public Interface **/
    $.KNMonitor = {

        isOnline : function() {
            var networkState = navigator.connection.type;
            if (networkState == "none" || networkState == "unknown") {
                return false;
            }else {
                return true;
            }
         },

        registerOnlineCallback : function(processor, callback) {
            onlineCallbacks[processor] = callback;
        },

        removeOnlineCallback : function(processor) {
            delete onlineCallbacks[processor];
        },

        registerOfflineCallback : function(processor, callback) {
            offlineCallbacks[processor] = callback;
        },

        removeOfflineCallback : function(processor) {
            delete offlineCallbacks[processor];
        },

        initialize : function(rootscope) {
            if(!initialized) {
                rootscope.$on("onOnline", function(event, data) {
                    try {
                        var key;
                        for(key in onlineCallbacks ) {
                            if(onlineCallbacks.hasOwnProperty(key)) {
                                onlineCallbacks[key]();
                            }
                        }
                    }catch(e) {
                        //alert ("Exception while callback.." + e);
                    }
                });
                rootscope.$on("onOffline", function(event, data) {
                    try {
                        var key;
                        for(key in offlineCallbacks ) {
                            if(offlineCallbacks.hasOwnProperty(key)) {
                                offlineCallbacks[key]();
                            }
                        }
                    }catch(e) {
                        //alert ("Exception while callback.." + e);
                    }
                });
                initialized = true;
            }
        }

    };
})();


/** Kryptos Local Storage **/
(function(){
    //var processors = {};

    $.KStorage = {

        set : function(processor, key, value) {
            var processors = $.jStorage.get("kprs");
            if(processors == null) {
                processors = {};
            }
            if(processors[processor]) {
                processors[processor][key] = value;
            }else {
                processors[processor] = {};
                processors[processor][key] = value;
            }
            $.jStorage.set(processor + "-" + key, value);
            $.jStorage.set("kprs", processors);
        },

        get : function(processor, key) {
            return $.jStorage.get(processor + "-" + key);
        },

        flush : function(processor) {
            var processors = $.jStorage.get("kprs");
            if(processors != null) {
                if(processors[processor]) {
                    for(key in processors[processor]) {
                        $.jStorage.deleteKey(processor + "-" + key);
                    }
                    delete processors[processor];
                }
            }
        },

        flushAll : function() {
            var processors = $.jStorage.get("kprs");
            if(processors != null) {
                for(processor in processors) {
                    flush(processor);
                }
            }
        }
    };
})();


MyCampusApp.init();